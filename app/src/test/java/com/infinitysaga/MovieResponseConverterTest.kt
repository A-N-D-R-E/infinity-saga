package com.infinitysaga

import com.infinitysaga.network.converter.MovieResponseConverter
import com.infinitysaga.network.response.MovieResponse
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MovieResponseConverterTest {
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun convertEmptyListTest(){
        val converter = MovieResponseConverter()
        val movieList = converter.convert(emptyList())

        assertTrue(movieList.isEmpty())
    }

    @Test
    fun convertMovieResponseTest(){
        val movieResponse = Mockito.mock(MovieResponse::class.java)
        val converter = MovieResponseConverter()
        val movieList = converter.convert(listOf(movieResponse))

        assertTrue(movieList.isNotEmpty())
    }
}