package com.infinitysaga

import com.infinitysaga.utils.SortMovieType
import org.junit.Assert
import org.junit.Test

class SortMovieTypeTest {
    @Test
    fun getTypeForEmptyValueTest(){
        val sortMovieType = SortMovieType.getType("")
        Assert.assertTrue(sortMovieType == SortMovieType.Default)
    }

    @Test
    fun getTypeForNullValueTest(){
        val sortMovieType = SortMovieType.getType(null)
        Assert.assertTrue(sortMovieType == SortMovieType.Default)
    }

    @Test
    fun getTypeForDefaultValueTest(){
        val typeValue = SortMovieType.Default.value
        val sortMovieType = SortMovieType.getType(typeValue)
        Assert.assertTrue(sortMovieType == SortMovieType.Default)
    }

    @Test
    fun getTypeForAlphabeticalValueTest(){
        val typeValue = SortMovieType.Alphabetical.value
        val sortMovieType = SortMovieType.getType(typeValue)
        Assert.assertTrue(sortMovieType == SortMovieType.Alphabetical)
    }

    @Test
    fun getTypeForRatedValueTest(){
        val typeValue = SortMovieType.Rated.value
        val sortMovieType = SortMovieType.getType(typeValue)
        Assert.assertTrue(sortMovieType == SortMovieType.Rated)
    }
}