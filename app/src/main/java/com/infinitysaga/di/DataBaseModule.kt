package com.infinitysaga.di

import android.app.Application
import androidx.room.Room
import com.infinitysaga.persistence.AppDatabase
import com.infinitysaga.persistence.dao.MovieDao
import com.infinitysaga.persistence.dao.MovieRatingDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class DataBaseModule {

    @Provides
    @Singleton
    fun provideAppDataBase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "infinity_saga.db").build()
    }

    @Provides
    @Singleton
    fun provideMovieDao(db: AppDatabase): MovieDao {
        return db.movieDao()
    }

    @Provides
    @Singleton
    fun provideMovieRatingDao(db: AppDatabase): MovieRatingDao {
        return db.movieRatingDao()
    }
}