package com.infinitysaga.utils

sealed class SortMovieType {
    open val value = ""
    companion object {
        const val Key = "sort_by"

        fun getType(value: String?): SortMovieType {
            return when(value){
                Alphabetical.value -> {
                    Alphabetical
                }
                Rated.value -> {
                    Rated
                }
                else -> {
                    Default
                }
            }
        }

    }

    object Alphabetical : SortMovieType(){ override val value = "Sort by alphabetical"}
    object Rated : SortMovieType(){ override val value = "Sort by rated"}
    object Default : SortMovieType(){ override val value = "Sort by year"}
}