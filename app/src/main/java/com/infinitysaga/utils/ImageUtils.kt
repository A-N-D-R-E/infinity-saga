package com.infinitysaga.utils

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.infinitysaga.R

object ImageUtils {
    fun loadImage(imageView: ImageView, url: String, onComplete: (()->Unit)? = null) {
        Glide.with(imageView.context)
            .load(GlideUrl(url))
            .apply(getRequestOptions())
            .listener(getGlideRequestListener(onComplete))
            .into(imageView)
    }

    private fun getGlideRequestListener(onComplete: (() -> Unit)?): RequestListener<Drawable> {
        return object : RequestListener<Drawable> {
            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean
            ): Boolean {
                onComplete?.invoke()
                return false
            }

            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean
            ): Boolean {
                onComplete?.invoke()
                return false
            }
        }
    }

    private fun getRequestOptions() = RequestOptions().apply {
        error(R.color.black)
        skipMemoryCache(false)
        diskCacheStrategy(DiskCacheStrategy.RESOURCE)
    }
}