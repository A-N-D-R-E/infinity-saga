package com.infinitysaga.utils

import android.util.Log
import com.infinitysaga.BuildConfig
object Logger {
    fun print(throwable: Throwable?){
        if(BuildConfig.DEBUG){
            Log.e("### begin error", throwable?.message ?: "")
            throwable?.printStackTrace()
        }
    }
}
