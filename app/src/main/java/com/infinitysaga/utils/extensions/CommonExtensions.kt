package com.infinitysaga.utils.extensions

import android.os.Handler
import android.os.Looper

fun runOnUiThread(runnable: ()-> Unit){
    Handler(Looper.getMainLooper()).post(runnable)
}