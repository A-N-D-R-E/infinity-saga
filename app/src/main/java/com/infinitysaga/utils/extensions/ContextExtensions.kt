package com.infinitysaga.utils.extensions

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.infinitysaga.utils.SortMovieType

fun Context.getMoviePreferences(): SharedPreferences? {
    return getSharedPreferences("movie_preferences", MODE_PRIVATE)
}

fun Context.getSelectedSortType(): SortMovieType {
    val sortBy = getMoviePreferences()?.getString(SortMovieType.Key, SortMovieType.Default.value)
    return SortMovieType.getType(sortBy)
}