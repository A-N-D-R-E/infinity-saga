package com.infinitysaga.interfaces

interface MovieItemHandler {
    fun onRatingChange(rating: Int, movieId: String)
}