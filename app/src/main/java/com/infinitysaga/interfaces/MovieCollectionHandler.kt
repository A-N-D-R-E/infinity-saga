package com.infinitysaga.interfaces

interface MovieCollectionHandler {
    fun onFilterClick()
}