package com.infinitysaga.network.converter

import com.infinitysaga.persistence.model.Movie
import com.infinitysaga.network.response.MovieResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieResponseConverter @Inject constructor () {
    private fun convert(movieResponse: MovieResponse): Movie{
        with(movieResponse){
            return Movie(
                id = makeMovieId(this),
                title = title,
                year = year,
                rated = rated,
                released = released,
                runtime = runtime,
                genre = genre,
                director = director,
                writer = writer,
                actors = actors,
                plot = plot,
                poster = poster
            )
        }
    }

    private fun makeMovieId(movieResponse: MovieResponse): String {
        return movieResponse.title?.replace(" ", "").plus(movieResponse.year)
    }

    fun convert(movieResponseList: List<MovieResponse>): List<Movie>{
        val movieList = ArrayList<Movie>()
        movieResponseList.forEach {movieResponse->
            movieList.add(convert(movieResponse))
        }

        return movieList
    }
}