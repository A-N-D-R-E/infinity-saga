package com.infinitysaga.network

import com.infinitysaga.network.response.MovieResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET

interface RetrofitApi {
    @GET("saga")
    fun getMovies(): Observable<List<MovieResponse>>
}