package com.infinitysaga.network.response

data class MovieResponse(
    var title: String?,
    var year: String?,
    var rated: String?,
    var released: String?,
    var runtime: String?,
    var genre: String?,
    var director: String?,
    var writer: String?,
    var actors: String?,
    var plot: String?,
    var poster: String?
)