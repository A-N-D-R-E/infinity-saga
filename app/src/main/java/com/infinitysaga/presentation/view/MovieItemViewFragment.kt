package com.infinitysaga.presentation.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.infinitysaga.databinding.MovieItemViewFragmentBinding
import com.infinitysaga.presentation.viewmodel.MovieItemViewViewModel
import com.infinitysaga.utils.ImageUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieItemViewFragment : BaseFragment() {
    private val viewModel: MovieItemViewViewModel by viewModels()
    private lateinit var binding: MovieItemViewFragmentBinding
    private val args: MovieItemViewFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = MovieItemViewFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        setToolbar(binding.toolBar)
        enableUpButton()
        initObservers()
    }

    private fun initObservers(){
        viewModel.getMovie(args.movieId).observe(viewLifecycleOwner, Observer {
            if (it != null) {
                binding.movie = it
                binding.executePendingBindings()
            }
        })
    }

}