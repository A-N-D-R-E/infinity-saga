package com.infinitysaga.presentation.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.infinitysaga.R
import com.infinitysaga.databinding.MovieCollectionFragmentBinding
import com.infinitysaga.interfaces.MovieCollectionHandler
import com.infinitysaga.interfaces.MovieItemHandler
import com.infinitysaga.presentation.adapter.MovieRecyclerViewAdapter
import com.infinitysaga.presentation.dialog.SortMovieDialog
import com.infinitysaga.presentation.viewmodel.MovieViewModel
import com.infinitysaga.presentation.viewmodel.state.MovieViewModelState
import com.infinitysaga.utils.SortMovieType
import com.infinitysaga.utils.extensions.getSelectedSortType
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieCollectionFragment : BaseFragment(), MovieItemHandler, MovieCollectionHandler {
    private lateinit var binding: MovieCollectionFragmentBinding
    private val movieViewModel by viewModels<MovieViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = MovieCollectionFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.handler = this
        binding.sortedType = getSortType()
        initRecyclerView()
        initSwipeRefresh()
        initSearchView()
        initObservers()
    }

    private fun initSearchView(){
        with(binding.searchBar){
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    newText?.let {
                        movieViewModel.search(it)
                    }
                    return true
                }

            })
        }
    }

    private fun initObservers(){
        movieViewModel.getMuvies().observe(viewLifecycleOwner, Observer { movies->
            if (isResumed) {
                binding.apply {
                    swipeRefresh.isRefreshing = false
                    movieList = movies
                    executePendingBindings()
                }
            }
        })

        movieViewModel.state.observe(viewLifecycleOwner, Observer {state->
            if (isResumed) {
                if (state is MovieViewModelState.ErrorSyncMovies) {
                    binding.swipeRefresh.isRefreshing = false
                    toast(R.string.error_on_sync_movies)
                }
            }
        })
    }

    private fun initRecyclerView(){
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = MovieRecyclerViewAdapter(this@MovieCollectionFragment, getSortType())
        }
    }

    private fun initSwipeRefresh(){
        binding.swipeRefresh.setOnRefreshListener {
            movieViewModel.syncMovies()
        }
    }

    override fun onRatingChange(rating: Int, movieId: String) {
        movieViewModel.rateMovie(movieId, rating)
    }

    override fun onFilterClick() {
        show(SortMovieDialog(onItemSelected = {
            getSortType().let {type->
                binding.sortedType = type
                (binding.recyclerView.adapter as MovieRecyclerViewAdapter).setSortBy(type)
            }
        }))
    }

    private fun getSortType(): SortMovieType {
        return context?.getSelectedSortType() ?: SortMovieType.Default
    }

}