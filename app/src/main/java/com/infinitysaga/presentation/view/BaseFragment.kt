package com.infinitysaga.presentation.view

import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.google.android.material.appbar.MaterialToolbar
import com.infinitysaga.utils.extensions.runOnUiThread

open class BaseFragment: Fragment() {
    fun show(dialog: DialogFragment){
        runOnUiThread {
            dialog.show(childFragmentManager.beginTransaction(), dialog::class.java.simpleName)
        }
    }

    fun toast(message: Int){
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }

    fun setToolbar(toolBar: MaterialToolbar) {
        (activity as MainActivity?)?.setSupportActionBar(toolBar)
    }

    fun enableUpButton(){
        (activity as MainActivity?)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}