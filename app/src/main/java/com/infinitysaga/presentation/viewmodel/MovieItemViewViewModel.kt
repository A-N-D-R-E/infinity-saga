package com.infinitysaga.presentation.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.infinitysaga.persistence.model.Movie
import com.infinitysaga.repository.MovieRepository

class MovieItemViewViewModel @ViewModelInject constructor(
    private val repository: MovieRepository
): ViewModel() {

    fun getMovie(movieId: String): LiveData<Movie> {
        return repository.getMovieById(movieId)
    }
}