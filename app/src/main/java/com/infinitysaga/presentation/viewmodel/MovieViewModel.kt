package com.infinitysaga.presentation.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.infinitysaga.network.converter.MovieResponseConverter
import com.infinitysaga.persistence.model.Movie
import com.infinitysaga.persistence.model.MovieRating
import com.infinitysaga.presentation.viewmodel.state.MovieViewModelState
import com.infinitysaga.repository.MovieRepository
import com.infinitysaga.repository.RatingRepository
import com.infinitysaga.utils.Logger
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.observers.DisposableObserver
import io.reactivex.rxjava3.schedulers.Schedulers

class MovieViewModel @ViewModelInject constructor(
    private val movieRepository: MovieRepository,
    private val ratingRepository: RatingRepository,
    private val movieResponseConverter: MovieResponseConverter
): ViewModel() {
    private lateinit var movies: LiveData<List<Movie>>
    private val disposable = CompositeDisposable()
    private val searchQueryLiveData = MutableLiveData("")
    private val _state = MutableLiveData<MovieViewModelState>()
    val state: LiveData<MovieViewModelState> =_state

    init {
        setState(MovieViewModelState.None)
        initSearch()
    }

    fun search(query: String){
        searchQueryLiveData.value = query
    }

    private fun initSearch(){
        movies = Transformations.switchMap(searchQueryLiveData) { query->
            if(query.isNotBlank()) {
                movieRepository.searchMovies(query)
            }
            else{
                movieRepository.listMovies()
            }
        }
    }

    fun syncMovies(){
        disposable.add(
            movieRepository.getMuvies()
                .subscribeOn(Schedulers.io())
                .map { movieResponseList->
                    movieResponseConverter.convert(movieResponseList)
                }
                .observeOn(Schedulers.io())
                .subscribeWith(object : DisposableObserver<List<Movie>>(){
                    override fun onComplete() {
                        setState(MovieViewModelState.CompleteSyncMovies)
                    }

                    override fun onNext(movieList: List<Movie>) {
                        with(movieRepository){
                            deleteAll()
                            insertMovies(movieList)
                        }
                    }

                    override fun onError(e: Throwable) {
                        setState(MovieViewModelState.ErrorSyncMovies)
                        Logger.print(e)
                    }
            })
        )
    }

    fun rateMovie(movieId: String, rating: Int) {
        val movieRating = MovieRating(movieId, rating)
        disposable.add(
            Observable.fromCallable {
                ratingRepository.ratingMovie(movieRating)
            }.subscribeOn(Schedulers.io()).subscribe()
        )
    }

    fun getMuvies(): LiveData<List<Movie>> {
        return movies
    }

    override fun onCleared() {
        disposable.dispose()
        super.onCleared()
    }

    private fun setState(state: MovieViewModelState){
        _state.postValue(state)
    }
}