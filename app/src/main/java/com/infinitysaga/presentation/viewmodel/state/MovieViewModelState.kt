package com.infinitysaga.presentation.viewmodel.state

sealed class MovieViewModelState {
    object ErrorSyncMovies: MovieViewModelState()
    object CompleteSyncMovies: MovieViewModelState()
    object None: MovieViewModelState()
}