package com.infinitysaga.presentation.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.fragment.app.DialogFragment
import com.infinitysaga.R
import com.infinitysaga.databinding.SortMovieDialogLayoutBinding
import com.infinitysaga.utils.SortMovieType
import com.infinitysaga.utils.extensions.getMoviePreferences
import com.infinitysaga.utils.extensions.getSelectedSortType

class SortMovieDialog(private val onItemSelected: ()-> Unit): DialogFragment() {
    private lateinit var binding: SortMovieDialogLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.MaterialDialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = SortMovieDialogLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        checkSelectedSort()
        binding.sortRadioGroup.setOnCheckedChangeListener { radioGroup, checkedId ->
            onSortByChanged(radioGroup, checkedId)
            onItemSelected.invoke()
            dismiss()
        }
    }

    private fun onSortByChanged(radioGroup: RadioGroup, checkedId: Int) {
        val preferences = radioGroup.context.getMoviePreferences()
        preferences?.edit()?.let {
            when(checkedId){
                R.id.sort_by_alphabetical_order ->{
                    it.putString(SortMovieType.Key, SortMovieType.Alphabetical.value)?.apply()
                }
                R.id.sort_by_rated ->{
                    it.putString(SortMovieType.Key, SortMovieType.Rated.value)?.apply()
                }

                R.id.sort_by_default ->{
                    it.putString(SortMovieType.Key, SortMovieType.Default.value)?.apply()
                }
                else -> it.apply()
            }
        }
    }

    private fun checkSelectedSort(){
        when(context?.getSelectedSortType()){
            SortMovieType.Alphabetical ->{
                binding.sortRadioGroup.check(R.id.sort_by_alphabetical_order)
            }
            SortMovieType.Rated ->{
                binding.sortRadioGroup.check(R.id.sort_by_rated)
            }
            else ->{
                binding.sortRadioGroup.check(R.id.sort_by_default)
            }
        }
    }
}