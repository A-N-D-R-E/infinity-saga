package com.infinitysaga.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RatingBar
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.infinitysaga.databinding.MovieItemListBinding
import com.infinitysaga.interfaces.MovieItemHandler
import com.infinitysaga.persistence.model.Movie
import com.infinitysaga.presentation.view.MovieCollectionFragmentDirections
import com.infinitysaga.utils.SortMovieType

class MovieRecyclerViewAdapter(private val handler: MovieItemHandler, sortMovieType: SortMovieType?): RecyclerView.Adapter<MovieRecyclerViewAdapter.MovieViewHolder>() {
    private val movieList = ArrayList<Movie>()
    private var sortBy = sortMovieType

    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = MovieItemListBinding.inflate(inflater, parent, false)
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(movieList[position])
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    fun updateMovieList(movies: List<Movie>) {
        when(sortBy){
            SortMovieType.Alphabetical ->{
                addAll(movies.sortedBy { it.title })
            }
            SortMovieType.Rated ->{
                addAll(movies.sortedByDescending { it.rating })
            }
            else ->{
                addAll(movies.sortedByDescending { it.year })
            }
        }
    }

    private fun addAll(sortedMovies: List<Movie>){

        with(movieList){
            clear()
            addAll(sortedMovies)
        }

        notifyDataSetChanged()
    }

    fun setSortBy(sortMovieType: SortMovieType){
        sortBy = sortMovieType
        updateMovieList(movieList)
    }

    inner class MovieViewHolder(private val binding: MovieItemListBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(movie: Movie){
            binding.movie = movie
            binding.cardView.setOnClickListener {
                onItemClick(movie)
            }
            binding.movieRatingBar.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener { _, rating, _ ->
                if (rating.toInt() != movie.rating) {
                    handler.onRatingChange(rating.toInt(), movie.id)
                }
            }
            binding.executePendingBindings()
        }

        private fun onItemClick(movie: Movie) {
            val action = MovieCollectionFragmentDirections.actionMovieCollectionFragmentToMovieItemViewFragment(movie.id)
            itemView.findNavController().navigate(action)
        }
    }
}