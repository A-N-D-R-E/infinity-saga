package com.infinitysaga.presentation.adapter

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toolbar
import androidx.appcompat.widget.SearchView
import androidx.core.view.get
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.MaterialToolbar
import com.infinitysaga.persistence.model.Movie
import com.infinitysaga.utils.ImageUtils
import com.infinitysaga.utils.extensions.runOnUiThread

class BindingAdapter {
    companion object {
        @JvmStatic
        @BindingAdapter("app:movies")
        fun bindMovieList(recyclerView: RecyclerView, list: List<Movie>?){
            list?.let{
                (recyclerView.adapter as MovieRecyclerViewAdapter).updateMovieList(it)
            }
        }

        @JvmStatic
        @BindingAdapter("app:expandSearchOnTouch")
        fun SearchView.bindExpandSearchOnTouch(expand: Boolean){
            if (expand){
                post {
                    setOnClickListener {
                        isIconified = false
                    }
                    if(query.isNotBlank()){
                        isIconified = false
                    }
                }
            }
        }

        @JvmStatic
        @BindingAdapter("app:emptyMessageVisibility")
        fun bindEmptyMessageVisibility(emptyMessage: TextView, list: List<Movie>?){
            runOnUiThread {
                if(list?.isEmpty() == true){
                    emptyMessage.visibility = View.VISIBLE
                }
                else{
                    emptyMessage.visibility = View.GONE
                }
            }
        }


        @JvmStatic
        @BindingAdapter(value = ["loadPicture", "progressBar"], requireAll = false)
        fun ImageView.loadPicture(url: String?, progress: ProgressBar?) {
            url?.let {
                ImageUtils.loadImage(this, it, onComplete = {
                    runOnUiThread {
                        progress?.visibility = View.GONE
                    }
                })
            }
        }
    }
}