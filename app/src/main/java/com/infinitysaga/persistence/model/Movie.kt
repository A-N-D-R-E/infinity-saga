package com.infinitysaga.persistence.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Movie(
    @PrimaryKey var id: String = "",
    @ColumnInfo var title: String? = "",
    @ColumnInfo var year: String? = "",
    @ColumnInfo var rated: String? = "",
    @ColumnInfo var released: String? = "",
    @ColumnInfo var runtime: String? = "",
    @ColumnInfo var genre: String? = "",
    @ColumnInfo var director: String? = "",
    @ColumnInfo var writer: String? = "",
    @ColumnInfo var actors: String? = "",
    @ColumnInfo var plot: String? = "",
    @ColumnInfo var poster: String? = ""
){
    @ColumnInfo var rating: Int = 0
}