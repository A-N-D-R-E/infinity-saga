package com.infinitysaga.persistence.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MovieRating(
    @PrimaryKey var movieId: String = "",
    @ColumnInfo var rating: Int = 0
)