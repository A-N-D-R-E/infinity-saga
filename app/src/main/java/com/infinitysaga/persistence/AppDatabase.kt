package com.infinitysaga.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.infinitysaga.persistence.dao.MovieDao
import com.infinitysaga.persistence.dao.MovieRatingDao
import com.infinitysaga.persistence.model.Movie
import com.infinitysaga.persistence.model.MovieRating

@Database(
    entities = [
        Movie::class,
        MovieRating::class
    ],
    version = 1,
    exportSchema = false
)

abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
    abstract fun movieRatingDao(): MovieRatingDao
}