package com.infinitysaga.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.infinitysaga.persistence.model.Movie

@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg movie: Movie)

    @Query(
    """SELECT M.id, m.title, M.year, M.rated, M.released, M.runtime, M.genre, M.director, M.writer, M.actors, M.plot, M.poster, 
             CASE WHEN R.rating IS NOT NULL THEN R.rating ELSE 0 END AS rating 
             FROM movie AS M 
             LEFT JOIN movieRating AS R 
             ON M.id = R.movieId"""
    )
    fun list(): LiveData<List<Movie>>

    @Query(
    """SELECT M.id, m.title, M.year, M.rated, M.released, M.runtime, M.genre, M.director, M.writer, M.actors, M.plot, M.poster, 
             CASE WHEN R.rating IS NOT NULL THEN R.rating ELSE 0 END AS rating 
             FROM movie AS M 
             LEFT JOIN movieRating AS R 
             ON M.id = R.movieId
             WHERE title LIKE :searchQuery 
             OR genre LIKE :searchQuery 
             OR released LIKE :searchQuery 
             OR director LIKE :searchQuery 
             OR writer LIKE :searchQuery"""
    )
    fun searchMovies(searchQuery: String): LiveData<List<Movie>>

    @Query("SELECT * from movie WHERE id=:id")
    fun getOneBykey(id: String): LiveData<Movie>

    @Query("DELETE from movie")
    fun deleteAll()

    @Query("DELETE from movie WHERE id=:id")
    fun delete(id: Int)
}