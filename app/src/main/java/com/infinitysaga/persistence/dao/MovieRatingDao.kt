package com.infinitysaga.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.infinitysaga.persistence.model.MovieRating

@Dao
interface MovieRatingDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg movieRating: MovieRating)

    @Query("SELECT * FROM movieRating")
    fun list (): LiveData<List<MovieRating>>

    @Query("SELECT * from movieRating WHERE movieId=:id")
    fun getOneBykey(id: String): LiveData<MovieRating>

    @Query("DELETE from movieRating")
    fun deleteAll()

    @Query("DELETE from movieRating WHERE movieId=:id")
    fun delete(id: Int)
}