package com.infinitysaga.repository

import com.infinitysaga.persistence.dao.MovieRatingDao
import com.infinitysaga.persistence.model.MovieRating
import javax.inject.Inject

class RatingRepository @Inject constructor(
    private val ratingDao: MovieRatingDao
) {

    fun ratingMovie(rating: MovieRating) {
        ratingDao.insert(rating)
    }
}