package com.infinitysaga.repository

import androidx.lifecycle.LiveData
import com.infinitysaga.network.RetrofitApi
import com.infinitysaga.network.response.MovieResponse
import com.infinitysaga.persistence.dao.MovieDao
import com.infinitysaga.persistence.model.Movie
import com.infinitysaga.utils.extensions.toSearchQuery
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val movieDao: MovieDao,
    private val retrofitApiService: RetrofitApi
) {

    fun getMuvies(): Observable<List<MovieResponse>> {
        return retrofitApiService.getMovies()
    }

    fun getMovieById(id: String): LiveData<Movie> {
        return movieDao.getOneBykey(id)
    }

    fun insertMovies(movieList: List<Movie>){
        movieDao.insert(*movieList.toTypedArray())
    }

    fun deleteAll(){
        movieDao.deleteAll()
    }

    fun listMovies(): LiveData<List<Movie>> {
        return movieDao.list()
    }

    fun searchMovies(query: String): LiveData<List<Movie>> {
        return movieDao.searchMovies(query.toSearchQuery())
    }
}